cat > humingzhe-csr.json <<EOF
{
  "CN": "humingzhe",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "BeiJing",
      "ST": "BeiJing"
    }
  ]
}
EOF

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes humingzhe-csr.json | cfssljson -bare humingzhe 

kubectl config set-cluster kubernetes \
  --certificate-authority=ca.pem \
  --embed-certs=true \
  --server=https://192.168.31.63:6443 \
  --kubeconfig=humingzhe-kubeconfig
  
kubectl config set-credentials humingzhe \
  --client-key=humingzhe-key.pem \
  --client-certificate=humingzhe.pem \
  --embed-certs=true \
  --kubeconfig=humingzhe-kubeconfig

kubectl config set-context default \
  --cluster=kubernetes \
  --user=humingzhe \
  --kubeconfig=humingzhe-kubeconfig

kubectl config use-context default --kubeconfig=humingzhe-kubeconfig

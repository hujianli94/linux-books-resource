#!/bin/bash
##__author__ is humingzhe
##email admin@humingzhe.com
#时间变量，用于记录日志
d=`date --date today +%Y%m%d_%H:%M:%S`
#计算 Nginx 进程数量
n=`ps -C nginx --no-heading|wc -l`
#如果进程数为 0，则启动 Nginx，并且再次检测 Nginx 进程数
#如果进程数还为 0，则说明 Nginx 无法启动，此时需要关闭 Keepalived
if [ $n -eq "0" ]; then
	/etc/init.d/nginx start
	n2=`ps -C nginx --no-heading|wc -l`
	if [ $n2 -eq "0" ]; then
		echo "$d nginx down,keepalived will stop" >> /var/log/check_ng.log
		systemctl stop keepalived
	fi
fi 